import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

// const requireContext = require.context('./modules', false, /.*\.js$/);
const url = window.location.href;
export default new Vuex.Store({
  state: {
    data: null,
    error: false
  },

  getters: {
    getEvent: (state) => state.data,
    getError: (state) => state.error
  },

  actions: {
    saveEvent({commit}, event) {
      return new Promise((resolve, reject) => {
        axios.post(`${url}/api/events`, event)
        .then(res => {
          commit('addEvent', res.data.data)
          resolve(res)
          
        }).catch(err => {
          commit('getError', true)
          reject(err)
        })
      })
    }
  },

  mutations: {
    addEvent: (state, event) => state.data = event,
    getError: (state, err) => state.error = err
  }
})
