<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'eventName' => $faker->text(50),
        'fromDate' => $faker->date(),
        'toDate' => $faker->date(),
        'dayEvents' => $faker->text(8),
    ];
});
